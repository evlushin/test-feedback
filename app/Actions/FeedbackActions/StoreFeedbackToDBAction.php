<?php

declare(strict_types = 1);

namespace App\Actions\FeedbackActions;

use App\Contracts\FeedbackContracts\StoreFeedbackContract;
use App\Models\Feedback;

class StoreFeedbackToDBAction implements StoreFeedbackContract
{
    const STORE_TO = 'mysql';

    public function __invoke(array $data): mixed
    {
        $feedback = Feedback::create($data);

        return $feedback->getAttributes();
    }
}
