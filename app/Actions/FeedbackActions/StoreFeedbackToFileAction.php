<?php

declare(strict_types = 1);

namespace App\Actions\FeedbackActions;

use App\Contracts\FeedbackContracts\StoreFeedbackContract;
use Illuminate\Support\Facades\Storage;

class StoreFeedbackToFileAction implements StoreFeedbackContract
{
    const STORE_TO = 'file';
    const DELIMITER = ';';

    public function __invoke(array $data): mixed
    {
        $fileName = Storage::disk(config('feedback.file.disk'))->path(config('feedback.file.filename'));

        if (file_exists($fileName)) {
            $file = fopen($fileName, 'a');
            fputcsv($file, $data, self::DELIMITER);
        } else {
            $file = fopen($fileName, 'w');
            $columns = array_keys($data);
            fputcsv($file, $columns, self::DELIMITER);
            fputcsv($file, $data, self::DELIMITER);
        }
        fclose($file);

        return [
            'filename' => $fileName,
            'filesize' => filesize($fileName),
        ];
    }
}
