<?php

declare(strict_types = 1);

namespace App\Providers;

use App\Actions\FeedbackActions\StoreFeedbackToDBAction;
use App\Actions\FeedbackActions\StoreFeedbackToFileAction;
use App\Contracts\FeedbackContracts\StoreFeedbackContract;
use Illuminate\Support\ServiceProvider;

class ActionServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
        $storeFeedbackType = config('feedback.store_to');

        switch ($storeFeedbackType) {
            case StoreFeedbackToFileAction::STORE_TO:
                $this->app->bind(StoreFeedbackContract::class, StoreFeedbackToFileAction::class);
                break;
            case StoreFeedbackToDBAction::STORE_TO:
            default:
                $this->app->bind(StoreFeedbackContract::class, StoreFeedbackToDBAction::class);
                break;
        }
    }
}
