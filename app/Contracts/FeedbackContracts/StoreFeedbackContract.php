<?php

declare(strict_types = 1);

namespace App\Contracts\FeedbackContracts;

interface StoreFeedbackContract
{
    public function __invoke(array $data): mixed;
}
