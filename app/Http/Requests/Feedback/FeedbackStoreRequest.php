<?php

declare(strict_types = 1);

namespace App\Http\Requests\Feedback;

use Illuminate\Foundation\Http\FormRequest;

class FeedbackStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'name' => ['required', 'string', 'max:255'],
            'phone' => ['required', 'string', 'max:20'],
            'message' => ['required', 'string', 'max:1000'],
        ];
    }

    /**
     * Get custom messages for validator errors.
     *
     * @return array
     */
    public function messages(): array
    {
        return [
            'name.required' => 'Ведите имя',
            'name.max' => 'Превышен лимит в 255 символов',
            'phone.required' => 'Ведите номер телефона',
            'phone.max' => 'Превышен лимит в 20 символов',
            'message.required' => 'Ведите сообщение',
            'message.max' => 'Превышен лимит в 1000 символов',
        ];
    }
}
