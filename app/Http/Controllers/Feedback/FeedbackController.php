<?php

declare(strict_types = 1);

namespace App\Http\Controllers\Feedback;

use App\Contracts\FeedbackContracts\StoreFeedbackContract;
use App\Http\Controllers\Controller;
use App\Http\Requests\Feedback\FeedbackStoreRequest;
use Illuminate\Support\Facades\Log;
use Throwable;

class FeedbackController extends Controller
{
    public function store(FeedbackStoreRequest $request, StoreFeedbackContract $storeFeedback)
    {
        $data = $request->validated();
        Log::debug(__METHOD__, [
            'request' => $data,
        ]);

        try {
            $result = $storeFeedback($data);
            Log::debug(__METHOD__, ['result' => $result]);

            return response()->noContent();
        } catch (Throwable $e) {
            Log::error(__METHOD__, [
                'message' => $e->getMessage(),
            ]);

            return response()->json(['message' => $e->getMessage()], 500);
        }
    }
}
