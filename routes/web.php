<?php

declare(strict_types = 1);

use App\Http\Controllers\Feedback\FeedbackController;
use Illuminate\Support\Facades\Route;

Route::prefix('feedback')->group(function () {
    Route::post('/', [FeedbackController::class, 'store'])->name('projects.store');
});
