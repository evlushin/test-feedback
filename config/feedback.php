<?php

declare(strict_types = 1);

return [

    // Настройка куда сохраняем feedback
    'store_to' => env('FEEDBACK_STORE_TO', 'mysql'),

    'file' => [
        'disk' => env('FEEDBACK_FILE_DISK', 'feedback'),
        'filename' => env('FEEDBACK_FILE_NAME', 'feedback.csv'),
    ],

];
